const holes = document.querySelectorAll('.hole');
let scoreBoard = document.querySelector('.score');
const moles = document.querySelectorAll('.mole');
const gift = document.querySelector('.gift');
let lastHole;
let giftBonus;
let timeUp = false;
let score = 0;

//create a function to make a random time for mole to pop from the hole
function randomTime(min, max) {
    return Math.round(Math.random() * (max - min) + min);
}

//Loi exponentielle de paramètre 1/median
function randomTimeExp(median){
    return -median*Math.log(Math.random());
}

function randomBernouilli(p){
    //Nombre aléatoire entre 0 et 1
    let lance = Math.random();
    if (lance<p){
        return 1;
    }
    else{
        return 0;
    }
}

function randomBinomiale(n, p){
    let res = 0;
    for(let i = 0; i < n; i++){
        res+=randomBernouilli(p);
    }
    return res;
}

function randomHole(holes){
    const index  =  randomUniformeDiscrete(8)/*randomBinomiale(7, 0.5)*/;
    //console.log(index);
    const hole = holes[index];

    //prevent same hole from getting the same number
    if (hole === lastHole){
        return randomHole(holes);
    }
    lastHole = hole;
    return hole;
}

function randomRademacher(alpha){
    let res = Math.random();
    if (res < alpha){
        return 1;
    }
    return -1;
}

function randomGeometrique(p, n){
    let x=1;
    while (randomBernouilli(p)==0){
        if(x<n){
            x+=1
        }
        else return 0; //si le nombre d'essai x dépasse le nombre n de secondes du jeu, le cadeau n'apparaît jamais
    }
    return x;
}

function giftPeep(){
    gift.classList.add('giftAnim');
    
    }

function randomUniformeDiscrete(n){
    let p1 = 1/n;
    let de = Math.random();
    let res=0;
    while(de > p1){
        res+=1;
        p1+=1/n;
    }
    return res;
}
    


function peep() {
    const time = randomTimeExp(1000); //get a random time to determine how long mole should peep
    //console.log(time);
    const hole = randomHole(holes); //get the random hole from the randomHole function
//    const mole = moles[1];
    hole.classList.add('up');//add the CSS class so selected mole can "pop up"
    setTimeout(() => {
        hole.classList.remove('up'); //make the selected mole "pop down" after a random time
        if(!timeUp) {    
            peep();
        }
    }, time);
}

function startGame() {
    document.querySelector(".menu").style.display = "none";
    document.querySelector(".game").style.display = "flex";
    document.querySelector(".score").style.display = "flex";
    
    scoreBoard.textContent = 0;
    timeUp = false;
    score = 0;
    peep();
    
    let gameTime = 30000;
    let giftStart= randomGeometrique(0.1, gameTime);
    giftBonus = randomBinomiale(8, 0.5);
    
    
    setTimeout(() => timeUp = true, gameTime);
    
   let timeLeft = gameTime/1000;

    
    // affiche le compteur
var interval = setInterval(function()
{
    document.querySelector(".time-left").innerHTML = "Time left : " + --timeLeft;
//    document.getElementById("cco").innerHTML = -- cc;
    if (30-timeLeft == giftStart){
        giftPeep();
    }
    else if (timeLeft == 0)
        clearInterval(interval);

}, 1000);
}

function wack(e){
    if(!e.isTrusted) return; //** new thing I learned */
    let gain = randomRademacher(0.8);
    let message = document.querySelector('.message');
    if(gain<0){
        message.classList.add('newImg');
    }
    

    //pour le cadeau
    gift.addEventListener("click", function() {        
        score+= giftBonus;
        //console.log(giftBonus);
        //console.log(score);
        gift.classList.remove('giftAnim');
      });

      score+=gain;

    this.parentNode.classList.remove('up'); //this refers to item clicked
    scoreBoard.textContent = score;
}

moles.forEach(mole => mole.addEventListener('click', wack))


